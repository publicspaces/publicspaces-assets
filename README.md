# PublicSpaces Web Assets

## Theme

```
cd theme
npm install
npm run dev
```

This will build the theme and watches for changes. The built CSS is copied over to `/docs/.vuepress/styles`. It will also serve a page at <http://localhost:5173/>

## Docs

```
cd docs
npm install
npm run dev
```

This will serve the  documentation at  <http://localhost:8080/> using [Vuepress](https://vuepress.vuejs.org/)


## Versioning and automatic deployments

Commits will be automaticly deployed to the PublicSpaces CDN. We use git tags to deploy specific versions.

The `main` branch deploys to: https://cdn.publicspaces.net/web/theme/style.css
The `develop` branch deploys to: https://cdn.publicspaces.net/web/theme/latest/style.css

Version tags like `v0.1.0` will be deployed to: https://cdn.publicspaces.net/web/theme/v0.1.0/style.css