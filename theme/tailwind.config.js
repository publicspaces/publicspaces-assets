/** @type {import('tailwindcss').Config} */
const forms = require('@tailwindcss/forms');

module.exports = {
  content: ['./src/**/*.{html,js,jsx,css}'],
  theme: {
    fontFamily: {
      'body': ['UncutSans', 'ui-sans-serif', 'system-ui'],
      'heading': ['Geomanist', 'ui-sans-serif', 'system-ui'],
      'mono': ['MartianMono', 'ui-monospace', 'system-ui'],
    },
    fontSize: {
      xs: '.5rem',
      sm: '.8rem',
      base: '1rem',
      lg: '1.5rem',
      xl: '2rem',
      xxl: '4rem'
    },
    lineHeight: {
      xs: '1.4em',
      sm: '1.2em',
      base: '1.4em',
      lg: '1.2em',
      xl: '1.2em',
      xxl: '1em',
    }
  },
  safelist: [
    'grid',
    {
      pattern: /^(grid|row|col|gap|justify)-/,
    },
  ],
  plugins: [
    forms
  ],
}
