# Getting started

To use the PublicSpaces theme and components, follow these steps:

## CDN
1. Include `<link rel="stylesheet" href="https://cdn.publicspaces.net/web/theme/style.css" />` tag in your `<head>` tag.

## Self hosted
1. Download the stylesheet <https://cdn.publicspaces.net/web/theme/style.css>.
2. Download the fonts from <https://cdn.publicspaces.net/web/theme/fonts.zip> and put them next to your `style.css` in the `fonts/` folder. 

## Module
The bundled theme is built with, but **does not include** [Tailwind](https://tailwindcss.com/). If you want to extend the theme with Tailwind, you need to include it in your project.

1. Clone <https://gitlab.com/publicspaces/publicspaces-assets.git>
2. Install dependencies with `npm install`
3. In `tailwind.config.js` make sure to include your template files in the `content` property so tailwind knows where to look
```js{2}
module.exports = {
  content: ['./src/**/*.{html,js,jsx,css}'],
  ...
```
4. Run `npm run dev` to start a dev server with hot reloading
5. Run `npm run build` to build for production
