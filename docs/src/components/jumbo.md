---
title: Jumbo
---

# Jumbo <Badge>themable</Badge>

```html
<section class="jumbo">
    <div class="text">
        <h1 class="title">Title</h1>
        <div class="excerpt">
            <p class="small">Description</p>
            <div class="cta">
                <a class="button button--inverted" href="#">Primary button</a>
                <a href="#">Secundary button</a>
            </div>
        </div>
    </div>
    <figure>
        <img src="https://picsum.photos/1920/1080" width="300" height="200" alt="" />
        <figcaption>Caption / Credit</figcaption>
    </figure>
</section>
```

## Options

### Layouts

This component has 5 different visual layouts that can be enabled by adding a modifier class.

1. image right (default)
2. image left `jumbo--image-left`
3. image bottom `jumbo--image-bottom`
4. image background `jumbo--image-background`
5. image none `jumbo--image-none`

```html{1}
<section class="jumbo jumbo--image-bottom">
    <div class="text">
        <h1 class="title">Title</h1>
        ...
```

::: tip
For the `jumbo--image-none` variation the `figure` element can be removed
:::

### Theming

This component can be themed by overriding the `--foreground` and `--background` variables through the `style` attribute.

```html{1}
<section class="jumbo" style="--background: color; --foreground: color">
    <div class="text">
        <h1 class="title">Title</h1>
        ...
```
