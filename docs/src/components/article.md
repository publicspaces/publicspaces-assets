---
title: Article
---

# Article

This component is typically used to display long form content like blog posts.

```html
<article class="article">
    <p>Na dit weekend zal PublicSpaces haar team aanvullen met enkele niet fysieke collega's. Door de snelle ontwikkelingen op het gebied van AI hebben we gemerkt dat veel van onze werkzaamheden zich goed lenen om door AI uitgevoerd te worden.</p>
    <figure>
        <img src="https://picsum.photos/1920/1080" width="300" height="200" alt="" />
        <figcaption>Caption / Credit</figcaption>
    </figure>
    <p>'Het is echt superfijn om te weten dat PublicSpaces 24/7 door kan gaan met haar missie. Zo ga je als CTO toch een heel stuk geruster slapen' aldus Toon Toetenel. Emma beaamt dit: 'Het is fijn dat je met een gerust hart even tussendoor kan wandelen of kan gaan sporten. Echt fijn om die vrijheid te hebben'.ƒ</p>
</article>
```

## Options

### Elements

The following children are aligned automatically:

-   `h1`
-   `h2`
-   `h3`
-   `h4`
-   `p`
-   `ul`
-   `ol`
-   `div`
-   `figure`
