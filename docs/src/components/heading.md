---
title: Heading
---

# Heading

Headings can be used to introduce [sections](/components/sections.html).

```html{2}
<section class="section">
    <h2 class="heading">Section title</h2>
    <!-- Components -->
</section>
```

But they can also be used in combination with [columns](/components/columns.html).

```html{4,8}
<section class="section">
    <section class="columns columns-3">
        <div class="column">
            <h3 class="heading">Column title</h3>
            <!-- Components -->
        </div>
        <div class="column">
            <h3 class="heading">Column title</h3>
            <!-- Components -->
        </div>
    </section>
</section>
```
