---
title: Call To Action
---

# Call To Action <Badge>themable</Badge>

This component is typically used at the end of an article to encourage a apecific action. Call to actions also work well in [columns](/components/columns.html).

Feel free to change the `<h3>` to other `<h*>` tags depending on the context, ideally not smaller than `h3`.

```html
<a class="call-to-action" href="#">
    <div class="text">
        <h3 class="title">Title</h3>
        <p class="button">Action</p>
    </div>
    <figure>
        <img src="https://picsum.photos/1920/1080" width="300" height="200" alt="" />
        <figcaption>Caption / Credit</figcaption>
    </figure>
</a>
```

::: tip
The `figure` element is optional
:::

## Options

### Layouts

This component has 2 layouts that can be enabled by adding a modifier class.

1. button left `call-to-action--button-left` (default)
2. button right `call-to-action--button-right`

```html{1}
<a class="call-to-action call-to-action--button-right" href="#">
    <div class="text">
        <h3 class="title">Title</h3>
        ...
```

### Theming

This component can be themed by overriding the `--foreground` and `--background` variables through the `style` attribute.

```html{1}
<a class="call-to-action" href="#" style="--background: color; --foreground: color">
    <div class="text">
        <h3 class="title">Title</h3>
        ...
```
