---
title: Form
---

# Form

This component provides an easy way to set up simple forms. It is based off <https://github.com/tailwindlabs/tailwindcss-forms>.

```html
<form class="form">
    <label>
        <span>Full name</span>
        <input type="text" placeholder="" />
    </label>
    <label>
        <span>Email address</span>
        <input type="email" placeholder="john@example.com" />
    </label>
    <label>
        <span>When is your event?</span>
        <input type="date" />
    </label>
    <label>
        <span>What type of event is it?</span>
        <select>
            <option>Corporate event</option>
            <option>Wedding</option>
            <option>Birthday</option>
            <option>Other</option>
        </select>
    </label>
    <label>
        <span>Additional details</span>
        <textarea rows="3"></textarea>
    </label>
    <label class="checkbox">
        <input type="checkbox" checked />
        <span>Email me news and special offers</span>
    </label>
</form>
```

## Options

### Theming

This component has an extra theme that can be enabled by adding the modifier class `form--solid`.

```html{1}
<form class="form form--solid">
    <label>
        <span>Full name</span>
```
