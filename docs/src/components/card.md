---
title: Card
---

# Card

This component is typically used as a link to articles and products, or as an image with description in groups of logo's for example. It features an image, title, description and meta field. Works well in columns.

Feel free to remove tags as you like, and change the `h4` tag to `h3` depending on the context.

```html
<a class="card" href="#">
    <figure>
        <img src="img.png" width="300" height="200" alt="alt" />
    </figure>
    <h4 class="title">Title</h4>
    <p class="description">description</p>
    <p class="meta">https://url.com</p>
</a>
```
