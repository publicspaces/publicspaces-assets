---
title: Li
---

# Li

Generic list item. Used for listing products or articles as an alternative to [cards](/components/card.html). Typically used in [columns](/components/columns.html).

```html
<ul>
    <li class="li">
        <div class="title">
            <h4 class="title">Title</h4>
            <p class="meta">Date /URL</p>
        </div>
        <div class="description">
            <p>Description</p>
            <p class="flex">
                <a class="button" href="#">Button</a>
            </p>
        </div>
    </li>
</ul>
```

## Options

### Layouts

This component has a horizontal layout which is more compact and can be enabled by adding the `li--horizontal` modifier class.

```html{2}
<ul>
    <li class="li li--horizontal">
        <div class="title">
            <h4 class="title">Title</h4>
            ...
```
