---
title: Button
---

# Button <Badge>themable</Badge>

Generic button to indicate a call to action. Typically used in [jumbo](/components/jumbo.html), [article](/components/article.html) and [li](/components/li.html) components.

```html
<a class="button" href="#">Button</a>
```

## Options

### Theming

This component has extra visual layouts that can be enabled by adding a modifier class.

1. inverted `button--inverted`
2. themed `button--themed`
3. themed inverted `button-themed-inverted`

```html
<a class="button button--inverted" href="#">Button</a>
<a class="button button--themed" href="#">Button</a>
<a class="button button--themed-inverted" href="#">Button</a>
```

### Icon

By default an arrow icon is appended to indicate a call to action. This icon can be changed by overriding the `--icon` variable.

```html{1}
<a class="button" style="--icon: '↗'" href="#">Button</a>
```
