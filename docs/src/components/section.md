---
title: Section
---

# Section

Sections act as a wrapper for other components and makes sure correct margins are applied. Sections are also needed to make the background white, as the `<html>` and `<body>` background colors can be changed through theming, to account for overscroll.

Typically these sections contain a [heading](/components/heading.html) component.

```html
<section class="section">
    <h2 class="heading">Section title</h2>
    <!-- Components -->
</section>
```

Heading components can be `h2`, `h3`, or `h4` tags, depending on the context.

```html{2-4}
<section class="section">
    <h2 class="heading">Section title</h2>
    <h3 class="heading">Section title</h3>
    <h4 class="heading">Section title</h4>
</section>
```