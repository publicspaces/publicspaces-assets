---
title: Header
---

# Header <Badge>themable</Badge>

```html
<header class="header">
    <a href="/">PublicSpaces</a>
    <nav>
        <input type="checkbox" id="menu_toggle" />
        <label for="menu_toggle">Menu</label>
        <ul>
            <li><a href="#">Item</a></li>
            <li><a href="#">Item</a></li>
            <li>
                <input type="checkbox" id="submenu_1" />
                <label for="submenu_1">Submenu</label>
                <ul>
                    <li><a href="#">Submenu item</a></li>
                    <li><a href="#">Submenu item</a></li>
                    <li><a href="#">Submenu item</a></li>
                </ul>
            </li>
            <li><a class="button button--inverted" href="#">Log in</a></li>
            <li><a href="#">NL ⚑</a></li>
        </ul>
    </nav>
</header>

...

<script>
    document.addEventListener('click', function (event) {
        if (event.target.closest('.header') && (event.target.tagName === 'INPUT' || event.target.tagName === 'LABEL')) {
            return;
        } else {
            var checkboxes = document.querySelectorAll('.header input[type="checkbox"]');
            for (var i = 0; i < checkboxes.length; i++) {
                checkboxes[i].checked = false;
            }
        }
    });
</script>
```

:::warning

-   Include the script to collapse the submenu's when clicking outside of them

:::

:::tip TODO

-   Search

:::

## Options

### Sticky

This component can be made sticky by adding the `header--sticky` class.

```html{1}
<header class="header header--sticky">
    <a href="/">PublicSpaces</a>
        <nav>
        ...
```

### Theming

This component can be themed by overriding the `--foreground` and `--background` variables through the `style` attribute.

```html{1}
<header class="header" style="--background: color; --foreground: color">
    <a href="/">PublicSpaces</a>
        <nav>
        ...
```

### Extending

- This component can also be used for child- and productpages. To add extra branding simply add the following

```html{2}
<header class="header">
    <a href="/">PublicSpaces <span>/ Product title</span></a>
        <nav>
        ...
```

- It's possible to add a call to action [button](/components/button.html). Use it for `log in` or `sign up` buttons, preferbly as last item in the menu. 
