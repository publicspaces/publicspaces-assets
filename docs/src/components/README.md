# Components

## Introduction

Each page is composed of multiple components. Typically a page is built up as follows:
- header (navigation)
- jumbo
- sections
- footer

Components are called by their `class name`.

## Theming

Some componenst can be **themed**. To set an overall theme for these components, simply override the `--foreground` add `--background` css variables by adding the `style="--background: color; --foreground: color"` attribute to your `<html>` tag.

These variables can also be overridden per themeable component.
