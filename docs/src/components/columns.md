---
title: Columns
---

# Columns

This components splits up a section in columns. A typical section has 2 or 3 columns.

```html{2}
<section class="section">
    <section class="columns columns--2">
        <div class="column">
            <!-- Components -->
        </div>
        <div class="column">
            <!-- Components -->
        </div>
    </section>
</section>
```

## Options

### Layouts

This component has 5 different layouts that can be enabled by adding a modifier class.

-   1 column `columns--1` (default)
-   2 column `columns--2`
-   3 column `columns--3`
-   4 column `columns--4`
-   6 column `columns--6`

The 2 column layout has an extra option to change the `column` widths from `1/2` and `1/2` to `1/3` and `2/3`. You can add the modifier classes `column--narrow` and `column--wide` to change the width. This layout it typically used do display [list items](/components/li.html) or a group of [cards](/components/card.html).

```html{3,6}
<section class="section">
    <section class="columns columns--2">
        <div class="column column--narrow">
            <!-- Components -->
        </div>
        <div class="column column--wide">
            <!-- Components -->
        </div>
    </section>
</section>
```

### Composition

The `columns` class can be applied to other tags, and it's also possible to nest `columns`. This is helpful for composing a more complex layout. For example, to create a section that displays sponsor logo's and has a descriptive text, we can do the following:

```html{2,7-16}
<section class="section">
    <section class="columns columns--2">
        <div class="column column--narrow">
            <!-- Text -->
        </div>
        <div class="column column--wide">
            <ul class="columns columns--4">
                <li><!-- Logo --></li>
                <li><!-- Logo --></li>
                <li><!-- Logo --></li>
                <li><!-- Logo --></li>
                <li><!-- Logo --></li>
                <li><!-- Logo --></li>
                <li><!-- Logo --></li>
                <li><!-- Logo --></li>
            </ul>
        </div>
    </section>
</section>
```
