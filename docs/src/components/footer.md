---
title: Footer
---

# Footer <Badge>themable</Badge>

Generic 4 column footer component.

```html
<footer class="footer">
    <div class="columns">
        <a class="brand" href="/">PublicSpaces</a>
        <ul>
            <!-- List items -->
        </ul>
        <ul>
            <!-- List items -->
        </ul>
        <div class="description">
            <p>Description</p>
        </div>
    </div>
</footer>
```

:::tip TODO

-   PublicSpaces badge

:::

## Options

### Theming

This component can be themed by overriding the `--foreground` and `--background` variables through the `style` attribute.

```html{1}
<footer class="footer" style="--background: color; --foreground: color">
    <div class="columns">
        <a class="brand" href="/">PublicSpaces</a>
        ...
```
