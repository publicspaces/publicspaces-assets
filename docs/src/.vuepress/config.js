import { defaultTheme } from 'vuepress';
import { getDirname, path } from '@vuepress/utils'
import { registerComponentsPlugin } from '@vuepress/plugin-register-components'
import { searchPlugin } from '@vuepress/plugin-search';

const __dirname = getDirname(import.meta.url)

export default {

  title: 'PublicSpaces Webcomponents',

  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  theme: defaultTheme({
    contributors: false,
    lastUpdated: false,
    navbar: [
      {
        text: 'Guide',
        link: '/guide/',
      },
      {
        text: 'Components',
        link: '/components/'
      },
      {
        text: 'Tools',
        link: '/tools/'
      }
    ],
    sidebar: {
      '/guide/': [
        {
          text: 'Guide',
          collapsable: true,
          children: [
            '/guide/README.md',
          ],
        },
      ],
      '/components/': [
        {
          text: 'Components',
          collapsable: true,
          children: [
            '/components/header.md',
            '/components/jumbo.md',
            '/components/section.md',
            '/components/heading.md',
            '/components/columns.md',
            '/components/article.md',
            '/components/card.md',
            '/components/call-to-action.md',
            '/components/button.md',
            '/components/li.md',
            '/components/form.md',
            '/components/footer.md',
          ]
        }
      ],
      '/tools/': [
        {
          title: 'Tools',
          collapsable: false,
          children: [
            '/tools/colorcomposer.md',
            '/tools/morsecomposer.md',
          ]
        }
      ],

    },
  }),

  plugins: [
    searchPlugin({
      locales: {
        '/': {
          placeholder: 'Search',
        }
      }
    }),
    registerComponentsPlugin({
      components: {
        Header: path.resolve(__dirname, './components/Header.vue'),
        Jumbo: path.resolve(__dirname, './components/Jumbo.vue'),
        ColorComposer: path.resolve(__dirname, './components/ColorComposer.vue'),
        MorseComposer: path.resolve(__dirname, './components/MorseComposer.vue'),
      },
    }),
  ],

  // deplyment
  dest: 'public',
  base: '/publicspaces-assets/'
}
